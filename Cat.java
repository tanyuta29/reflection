import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class Cat extends Animal {
    public static void main(String[] args) throws InvocationTargetException, IllegalAccessException {
        Animal animal = new Animal();
        changeInformation(animal, "Rex", "male", 2);
        printInfo(animal);
    }

    private static void printInfo(Animal animal) {
        System.out.printf(animal.getName(), animal.getGender(), animal.getAge());
    }

    public static void changeInformation(Animal animal, String name, String gender, int age) throws IllegalAccessException, InvocationTargetException {
        Class<? extends Animal> aClass = animal.getClass();
        for (Field field : aClass.getDeclaredFields()) {
            if ("name".equals(field.getName())) {
                field.setAccessible(true);
                Object oldValue = field.get(animal);
                field.set(animal, name);
                System.out.printf("Змінили старе значення %s на нове %s %n", oldValue, name);
            }
            if ("gender".equals(field.getName())) {
                field.setAccessible(true);
                Object oldValue = field.get(animal);
                field.set(animal, gender);
                System.out.printf("Змінили старе значення %s на нове %s %n", oldValue, gender);
            }
            if ("age".equals(field.getName())) {
                field.setAccessible(true);
                Object oldValue = field.get(animal);
                field.set(animal, age);
                System.out.printf("Змінили старе значення %s на нове %s %n", oldValue, age);
            }
        }
    }
}
