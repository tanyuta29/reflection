public class Animal {
    public String name = "Alice";
    protected int age = 1;
    private String gender = "female";

    public Animal(String name, int age, String gender) {
        this.age = age;
        this.gender = gender;
        this.name = name;
    }

    public Animal() {
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getGender() {
        return gender;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void setInformation(String name, String gender,int age){
        this.name=name;
        this.gender=gender;
        this.age=age;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
